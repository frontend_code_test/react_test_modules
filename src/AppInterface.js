import * as React from 'react';
import ReactDOM from 'react-dom';
import Button from '@mui/material/Button';

export class AppInterface {
	test() {
		alert('test')
	}

	init(options = {}) {
		ReactDOM.render(this.render(options), document.querySelector('#block_content_body'));

	}

	render(options = {})
	{
		return <Button variant="contained">Hello World</Button>;
	}
}